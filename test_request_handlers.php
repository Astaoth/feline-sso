<html>
  <head>
    <title>test de la bibli gérant les requêtes</title>
      <?php include 'head_html.php'; ?>
  </head>
  <body>
    <h1>Text file backend</h1>
    <ul>
    <?php //Set data to the current benchtests
	  $SSO_user_backend="file_text";
	  $SSO_user_file_text_path="test_ressources/users.txt";
	  $SSO_site_backend="file_text";
	  $SSO_site_file_text_path="test_ressources/sites.txt";
	  
	  echo "<li>SSO_user_backend = $SSO_user_backend</li>";
	  echo "<li>SSO_user_file_text_path = $SSO_user_file_text_path</li>";
	  echo "<li>SSO_site_backend = $SSO_site_backend</li>";
	  echo "<li>SSO_site_file_text_path = $SSO_site_file_text_path</li>";
       ?>
    </ul>
    <h2>Handler generation</h2>
    <ul>
    <?php 
       $user_handler = request_handler_make("user");
       $site_handler = request_handler_make("site");

       echo "<li>";
       if($user_handler == FALSE) {
         echo "user_handler FALSE";
       } else {
         echo "user_handler seems to be fine.";
       }
       echo "</li><li>";
       if($site_handler == FALSE) {
         echo "site_handler FALSE.";
       } else {
         echo "site_handler seems to be fine.";
       }
       ?>
    </ul>
    <h2>Request tests</h2>
    <h3>Search</h3>
    <ul>
    <?php
       //       request_handler_exec($type, $handler, $reqtype, $data)
       echo "<li>Search by username";
       echo "<ul>";
       echo "<li>username : alpha</li>";
       echo "</ul>";
       $data = array("username" => "alpha");
    var_dump($data);
    $reslt = request_handler_exec("user", $user_handler, "search", $data);
    echo "<p>Résultat : </p>";
    var_dump($reslt);
    echo "</li>";

echo "<li>Search by groupList 1";
  echo "<ul>";
    echo "<li>groupList : group2, group3</li>";
    echo "</ul>";
  $data = array("groupList" => array("group2", "group3"));
  var_dump($data);
  $reslt = request_handler_exec("user", $user_handler, "search", $data);
  echo "<p>Résultat : </p>";
  var_dump($reslt);
  echo "</li>";

echo "<li>Search by groupList 2";
  echo "<ul>";
    echo "<li>groupList : group2, group3</li>";
    echo "</ul>";
  $data = array("groupList" => array("group1", "group3"));
  var_dump($data);
  $reslt = request_handler_exec("user", $user_handler, "search", $data);
  echo "<p>Résultat : </p>";
  var_dump($reslt);
  echo "</li>";
       ?>
    </ul>
    <h3>Add</h3>
    <p>NOT IMPLEMENTED !</p>
    <h3>Del</h3>
    <p>NOT IMPLEMENTED !</p>
    <h3>Update</h3>
    <p>NOT IMPLEMENTED !</p>
    <hr />
    
    <h1>MySQLI</h1>
    <h2>Handler generation</h2>
    <h2>Request tests</h2>
    <h3>Search</h3>
    <h3>Add</h3>
    <p>NOT IMPLEMENTED !</p>
    <h3>Del</h3>
    <p>NOT IMPLEMENTED !</p>
    <h3>Update</h3>
    <p>NOT IMPLEMENTED !</p>
    <hr />

    <h1>SQLite3</h1>
    <h2>Handler generation</h2>
    <h2>Request tests</h2>
    <h3>Search</h3>
    <h3>Add</h3>
    <p>NOT IMPLEMENTED !</p>
    <h3>Del</h3>
    <p>NOT IMPLEMENTED !</p>
    <h3>Update</h3>
    <p>NOT IMPLEMENTED !</p>
    <hr />

    <h1>PGSQL</h1>
    <h2>Handler generation</h2>
    <h2>Request tests</h2>
    <h3>Search</h3>
    <h3>Add</h3>
    <p>NOT IMPLEMENTED !</p>
    <h3>Del</h3>
    <p>NOT IMPLEMENTED !</p>
    <h3>Update</h3>
    <p>NOT IMPLEMENTED !</p>
    <hr />

    <h1>PDO MySQL</h1>
    <h2>Handler generation</h2>
    <h2>Request tests</h2>
    <h3>Search</h3>
    <h3>Add</h3>
    <p>NOT IMPLEMENTED !</p>
    <h3>Del</h3>
    <p>NOT IMPLEMENTED !</p>
    <h3>Update</h3>
    <p>NOT IMPLEMENTED !</p>
    <hr />

    <h1>PDO SQLite</h1>
    <h2>Handler generation</h2>
    <h2>Request tests</h2>
    <h3>Search</h3>
    <h3>Add</h3>
    <p>NOT IMPLEMENTED !</p>
    <h3>Del</h3>
    <p>NOT IMPLEMENTED !</p>
    <h3>Update</h3>
    <p>NOT IMPLEMENTED !</p>
    <hr />

    <h1>PDO PostgreSQL</h1>
    <h2>Handler generation</h2>
    <h2>Request tests</h2>
    <h3>Search</h3>
    <h3>Add</h3>
    <p>NOT IMPLEMENTED !</p>
    <h3>Del</h3>
    <p>NOT IMPLEMENTED !</p>
    <h3>Update</h3>
    <p>NOT IMPLEMENTED !</p>
    <hr />

    <h1>LDAP</h1>
    <h2>Handler generation</h2>
    <p>NOT IMPLEMENTED !</p>
    <h2>Request tests</h2>
    <h3>Search</h3>
    <p>NOT IMPLEMENTED !</p>
    <h3>Add</h3>
    <p>NOT IMPLEMENTED !</p>
    <h3>Del</h3>
    <p>NOT IMPLEMENTED !</p>
    <h3>Update</h3>
    <p>NOT IMPLEMENTED !</p>
    <hr />

    <?php
//Exec a request on a given request_handler.
//$type : type of datas (user, site)
//$handler : request handler made by request_handler_exec
//$reqtype : type of the request (search, add, del, update)
//$datas : array with the data request : 
//         - add : the tuple data ; 
//         - del : the tuple datas which we want to del for a better match (mixed assoc/numerical array)
//         - search : tuple data for a better match (mixed assoc/numerical array) EG : {username : "username", email : "", password : "password", group : {0 : "group1", 1 : "group2"}}.
//         - update : in the array, each data tuple is an array with the old and new datas. EG : {username : {0 : username, 1 : username}, password : {0 : oldpassword, 1 : newpassword}, groups : {0 : old group list, 1 : new group list}}. The first data set will be used like with the search reqtype. Next, they will be replaced by the second data set.
//    About del/search : will match datas with all matching fields. A field will be matching if all the datas matches
// /!\ WITH SQL : each key must be an attribute name !!!
// With text file, the keys are not used but instead the column number
//return : TRUE, FALSE, or array with requested datas
//Array<Array<String>> result : function request_handler_exec($type, $handler, $reqtype, $data) 

//Build a request handler. Will be used to make requests on file, LDAP, or SQL db in the same way
//NB : Imap request (usables only for auth) are not handled here
//$type : db type (user or site)
//return a request handler
//request handler : function request_handler_make($type)


//Execute an SQL request
//Type : request on site or user datas
//Handler : database handler (resulting of sql_open)
//Request : a string containing the request
//Array<Array<String>> result : function sql_request($type, $handler, $request)


//Build an SQL handler
//Type : request on site or user datas
//DB handler : function sql_open($type)
       ?>
  </body>
</html>


