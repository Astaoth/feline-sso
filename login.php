<?php session_start(); ?>
<html>
  <head>
    <?php include 'head_html.php'; 
          sso_check_status("disconnected"); ?>
    <?php echo '<title>'.$SSO_title.' : connexion en cours</title>'; ?>
    <?php /**** Some usefull functions ***/

	  //Compare the given password with the stored one
	  function check_password($pwd) {
	    global $SSO_password_hash;
	    echo 'SSO_password_hash = '.$SSO_password_hash.'<br />';
	    if($SSO_password_hash === "plain") {
	      if($_POST["password"] === $pwd) return true;
	      else return false;
	    }
	    echo 'hashed password : '.hash($SSO_password_hash, $_POST["password"]).'<br />';
	    echo 'saved password : '.$pwd.'<br />';
	    if(hash($SSO_password_hash, $_POST["password"]) === $pwd) return true;
	    return false;
	  }
	  ?>
  </head>

  <body id="login">
    <?php include 'skin/'.$SSO_skin.'/header.php'; ?>

    <div id="login">
      <p>Connexion en cours ...</p>
    <?php
       //For checking at the end if we are logged or not
       $logged=-1;

       if($SSO_logfile_level >=5) {
         echo "SSO_password_hash : $SSO_password_hash<br />";
         echo "SSO_user_backend : $SSO_user_backend<br />";
       }
       
       if(!isset($_POST["login"]) || $_POST["login"] === "") {
         echo '<p>Login manquant, redirection vers la page de connexion.</p>';
         header('Location: index.php?msg=Login%20manquant');
       }
       if($SSO_logfile_level >= 5) echo 'LOGIN : -'.$_POST["login"].'-<br />';
       if(!isset($_POST["password"]) || $_POST["password"] === "") {
         echo '<p>Mot de passe manquant, redirection vers la page de connexion.</p>';
         header('Location: index.php?msg=Mot%20de%20passe%20manquant');
       }
       if($SSO_logfile_level >= 5) echo 'MDP : -'.$_POST["password"].'-<br />';

       if(isset($SSO_user_backend)) {
######################################### SWITCH ON SSO_BACKEND
         switch ($SSO_user_backend) {
######################################### FILE_TEXT
           case "file_text":
             if(!isset($SSO_user_file_text_path)) {
               sso_errors();
               sso_log("error", "Backend configuration", "Undefined SSO_user_file_text_path");
               break;
             }
             $login_file = fopen($SSO_user_file_text_path, "r");
             if($login_file == FALSE) {
               sso_errors();
               sso_log("error", "Backend configuration", "Unable to open SSO_user_file_text_path");
               break;
             }

             $logged=0;
             while(!feof($login_file) && $logged < 1) {
               $line = explode(';',fgets($login_file));
	       if($line[0] === $_POST["login"])
	         if(check_password($line[2])) {
		   $_SESSION["groups"] = array_map('trim',explode(',', $line[3]));
		   $_SESSION["groups"][] = "file_text";
		   $_SESSION["email"] = $line[1];
		   $logged=1;
		 }
             }
             break;
######################################### IMAP
           case "imap":
	     if(!extension_loaded("imap")) {
	       sso_errors();
               sso_log("error", "PHP configuration", "PHP imap extension is not loaded. Please enable it or choose an other backend.");
               break;
	     }
	     if($SSO_user_imap_server === "") {
	       sso_errors();
               sso_log("error", "Backend configuration", "Undefined SSO_user_imap_server");
               break;
	     }
	     if($SSO_user_imap_prot === "") {
	       sso_errors();
               sso_log("error", "Backend configuration", "Undefined SSO_user_imap_prot");
               break;
	     }
	     if($SSO_user_imap_prot !== "imap" && $SSO_user_imap_prot !== "pop3") {
	       sso_errors();
               sso_log("error", "Backend configuration", "Bad definition of SSO_user_imap_prot");
               break;
	     }
	     if($SSO_user_imap_cert_validate !== TRUE && $SSO_user_imap_cert_validate !== FALSE ) {
	       sso_errors();
               sso_log("error", "Backend configuration", "Bad definition of SSO_user_imap_cert_validate");
               break;
	     }

	     $imap_flags = "";
	     $imap_url = "";
	     $mbox = "";
	     if($SSO_user_imap_ssl === "notls") {$imap_flags = $imap_flags."/notls";}
	     else if($SSO_user_imap_ssl === "tls") {$imap_flags = $imap_flags."/tls";}
	     else if($SSO_user_imap_ssl === "ssl") {$imap_flags = $imap_flags."/ssl";}

	     if($SSO_user_imap_cert_validate) $imap_flags = $imap_flags . "/validate-cert";
	     else $imap_flags = $imap_flags . "/novalidate-cert";
	     
	     $imap_url = $SSO_user_imap_server;
	     if($SSO_user_imap_port !== "") {$imap_url = $imap_url.":".$SSO_user_imap_port;}

	     $mbox = imap_open("{".$imap_url."/".$SSO_user_imap_prot.$imap_flags."}INBOX", $_POST["login"], $_POST["password"]);

	     if(!$mbox) {
	       $logged=0;
	     } else {
	       $_SESSION["groups"][] = "imap";
	       $_SESSION["email"] = $_POST["login"];
	       $logged=1;
	     }
	     echo "loggin done : $logged<br>";
	     imap_close($mbox);
             break;
######################################### LDAP
           case "ldap":
             break;
######################################### SQLITE
           case "sqlite":
	     $db = sql_open("user");

	     $logged=0;
	     $db_mdp = sql_request("user", $db, 'SELECT password FROM sso_users WHERE username == \''.$_POST["login"].'\'');

//	     if($SSO_logfile_level >= 5) echo '<br />Request will be executed<br />';
//	     $db_mdp = sqlite_query($db, sqlite_escape_string('SELECT password FROM sso_users WHERE username == \''.$_POST["login"].'\''));
//	     if($SSO_logfile_level >= 5) echo 'Request result dump : <br />';
//	     var_dump($db_mdp);


//	     if(sqlite_num_rows($db_mdp) != 0) {
	     if(count($db_mdp) != 0) {
	       $i=-1;

	       //	       while(sqlite_has_more($db_mdp)) {
//	       foreach(sqlite_fetch_all($db_mdp, SQLITE_ASSOC) as &$cur_mdp) {
	       foreach($db_mdp as &$cur_mdp) {
	         $i++;
		 //$cur_mdp = sqlite_fetch_array($db_mdp);
		 //  	     	 if(check_password($cur_mdp[0])) {
		 if(check_password($cur_mdp["password"])) {
//		   foreach(sqlite_fetch_all(sqlite_query($db, sqlite_escape_string('SELECT groups FROM users WHERE username == \''.$POST["login"].'\'')), SQLITE_ASSOC) as &$grp) {
		   foreach(sql_request("user", $db, sqlite_escape_string('SELECT groups FROM users WHERE username == \''.$POST["login"].'\'')) as &$grp) {
		     $_SESSION["groups"][] = $grp[groups];
		   }
		   var_dump($_SESSION["groups"]);
		   $_SESSION["groups"][] = "sqlite";
		   $logged=1;
		   break;
	         }
	       }
	     }

             break;
######################################### MYSQL
           case "mysql":
             break;
######################################### POSTGRESQL
           case "postgresql":
             break;
######################################### UNKNOWN BACKEND
           default:
             sso_errors();
             sso_log("error", "Backend configuration", "Unknown defined backend");
         }
######################################### ENDSWITCH

         if($logged > 0) {
           session_start();
      
           $_SESSION["login"] = $_POST["login"];
           $_SESSION["password"] = $_POST["password"];
           echo 'ids de session : '.$_SESSION["login"].' - '.$_SESSION["password"].'<br />';
           echo '<p>Connecté, redirection vers l\'accueil.</p>';
           header('Location: accueil.php');
         } else if ($logged == 0){
           echo '<p>Identifiants incorrects, redirection vers la page de connexion.</p>';
           header('Location: index.php?msg=Identifiants%20incorrects');
         }
       } else {
         sso_errors();
         sso_log("error", "Backend configuration", "Undefined backend");
       }
       ?>
    </div>

    <?php include 'skin/'.$SSO_skin.'/footer.php'; ?>
  </body>
</html>
