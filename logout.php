<?php session_start(); ?>
<html>
  <head>
    <?php $current_page_name = "Déconnexion";
	  include 'head_html.php'; 
          sso_check_status("connected"); ?>
  </head>

  <body id="logout">
    <?php include 'skin/'.$SSO_skin.'/header.php'; ?>

    <div id="logout">
      <p>Déconnexion en cours ...</p>
      <?php if(session_destroy()) {
	      header('Location: index.php'); 
	    } else {
	      sso_errors();
              sso_log("error", "Session", "Unable to destroy session");
	    }?>
    </div>

    <?php include 'skin/'.$SSO_skin.'/footer.php'; ?>
  </body>
</html>
