<?php
session_start();
include 'head_html.php'; 
sso_check_status("connected");
?>


<?php
  //Curl usage : http://stackoverflow.com/questions/20064271/how-to-use-basic-authorization-in-php-curl

$_SESSION["current_login"] = $_SESSION["login"];
$_SESSION["current_password"] = $_SESSION["password"];

$request = curl_init($_SESSION["current_url"]);

//timeout after 30 seconds - TODO : make a option
curl_setopt($request, CURLOPT_TIMEOUT, 30);

//if true, don't print data, return them with curl_exec
curl_setopt($request, CURLOPT_RETURNTRANSFER,FALSE);

//paramètres du choix du système d'auth : 
// tous : CURLAUTH_ANY
// basic web auth : CURLAUTH_BASIC
// digest : CURLAUTH_DIGEST
// uniquement les sûr : CURLAUTH_ANYSAFE
curl_setopt($request, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
curl_setopt($request, CURLOPT_USERPWD, $_SESSION["current_login"].':'.$_SESSION["current_password"]);

$status_code = curl_getinfo($request, CURLINFO_HTTP_CODE);   //get status code
$result=curl_exec ($request);
curl_close ($request);
$_SESSION["current_login"] = "";
$_SESSION["current_password"] = "";
?>
