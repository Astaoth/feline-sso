<?php
#Language selection
$SSO_language="fr";

#Page title
$SSO_title="Feline SSO";

#Page sub-title
$SSO_subtitle="Le SSO facile";

#Logo
$SSO_logo="";

#Skin folder name
$SSO_skin="default";

#Email acontact admin - used by the "Forgotten Password" link
$SSO_admin_contact="admin@example.org";

#Logfile path
$SSO_logfile="felinesso.log";

#Timestamp format
/* Used in logfile and on the pages when there is an error.
** The format is defined with the php style.
*/
$SSO_timestamp_format="Y-m-d H:i:s";

#Logfile level
/* 0 = nothing
** 1 = error
** 2 = warning
** 3 = info
** 4 = custom type, displayed as written
** 5 = debug
*/
$SSO_logfile_level = 5;

#Possibility to add a new url from the ui on the accueil page
$SSO_add_new_url_from_ui=true;

#Allowed groups
#Wildcard : * : all groups
#           ! : restriction
# NB : with the Imap backend, this parameter is not applied
$SSO_add_new_url_allowed_groups="*";

#Allowed users
/* Wildcard : * : all users
**            ! : restriction
*/
$SSO_add_new_url_allowed_users="*";

#Possibility to add a new url from the ui on the accueil page of other users
$SSO_add_new_url_to_all_users_from_ui=true;

#Allowed users
/* Wildcard : * : all users
**            ! : restriction
*/
$SSO_add_new_url_to_all_users_allowed_users="alucard";

#Allowed groups
/* Wildcard : * : all groups
**            ! : restriction
** NB : with the Imap backend, this parameter is not applied
*/
$SSO_add_new_url_to_all_users_allowed_groups="admin";


#How the password is hashed.
/* Supported hash methods are the one supported by php and "plain" (not hashed). The most popular are : 
** - md5 (not secured)
** - sha1
** - sha256
** - sha384
** - sha512
** - plain : no hash used
*/
$SSO_password_hash="md5";

#Identification backend. 
/* It could be : 
**  - file_text : clear file texte
**  - ldap : ldap
**  - imap : imap valid account
**  - sqlite : sqlite database
**  - mysql : mysql database
**  - postgresql : postgresql database
** Only one backend can be defined.
*/
#$SSO_user_backend="file_text";
$SSO_user_backend="sqlite";

#Identification backend configuration
/* Config backend examples : 
## Clear file text ("file_text")
# Define the file text path
$SSO_user_file_text_path="config/id.txt";

## IMAP
# Define the IMAP server address. It could be an ip address or a tld.
$SSO_user_imap_server="";

# Define the remote IMAP port. Default is 143.
$SSO_user_imap_port="143";

# SSL parameters. Possible values : 
#     - "nossl" or "" : SSL will not be used
#     - "ssl" : use SSL
#     - "tls" : force the Start-TLS cypher and reject the connection if the remote server doesn't support it
#     - "notls" : never use Start-TLS even with the server which support it
$SSO_user_imap_ssl="ssl";

#Validate the ssl certificate or not
#Possible values : TRUE, FALSE
$SSO_user_imap_cert_validate=TRUE;

# Define the protocol to use. 
# Possible values are "imap", "pop3".
# If void, it will be defined to "imap".
$SSO_user_imap_prot="imap";

#SQLite : 
$SSO_user_sqlite_path="config/felinesso.db";

MySQL :
$SSO_user_mysql_user_name="";
$SSO_user_mysql_password="";
$SSO_user_mysql_server="";
$SSO_user_mysql_base="";
$SSO_user_mysql_port="";

PostgreSQL :
$SSO_user_postgresql_user_name="";
$SSO_user_postgresql_password="";
$SSO_user_postgresql_server="";
$SSO_user_postgresql_base="";
$SSO_user_postgresql_port="";

LDAP :
$SSO_user_ldap_server="";
$SSO_user_ldap_port="";
$SSO_user_ldap_group="";
*/

$SSO_user_file_text_path="config/id.txt";
$SSO_user_sqlite_path="config/felinesso.db";


#Get site backend. 
/* It could be : 
**  - file_text : clear file texte
**  - sqlite : sqlite database
**  - mysql : mysql database
**  - postgresql : postgresql database
** Only one backend can be defined.
*/
$SSO_site_backend="file_text";

#Identification backend configuration
/* Config backend examples : 
## Clear file text ("file_text")
# Define the file text path
$SSO_site_file_text_path="config/id.txt";

#SQLite : 
$SSO_site_sqlite_path="config/felinesso.db";

MySQL :
$SSO_site_mysql_user_name="";
$SSO_site_mysql_password="";
$SSO_site_mysql_server="";
$SSO_site_mysql_base="";
$SSO_site_mysql_port="";

PostgreSQL :
$SSO_site_postgresql_user_name="";
$SSO_site_postgresql_password="";
$SSO_site_postgresql_server="";
$SSO_site_postgresql_base="";
$SSO_site_postgresql_port="";

LDAP :
$SSO_site_ldap_server="";
$SSO_site_ldap_port="";
$SSO_site_ldap_group="";
*/

$SSO_site_file_text_path="config/access.csv";
?>