<?php session_start();?>
<html>
  <head>
    <?php include 'head_html.php'; 
          sso_check_status("connected"); ?>
    <?php echo '<title>'.$SSO_title.' : accueil</title>'; ?>
    <?php
      //Check if an user group is inside a group list, expected arrays
      function check_user_group_in_list($user_group_list, $check_group_list) {
        foreach($user_group_list as $ug) {
	  if(in_array($ug, $check_group_list)) return TRUE;
	}
	return FALSE;
      }

      ?>
  </head>

  <body id="welcome">
    <?php include 'skin/'.$SSO_skin.'/header.php'; ?>
    <?php echo 'Bienvenu '.$_SESSION["login"]; ?>

    <div id="welcome">
      <ul id="buttons">
	<li id="logout"><a href="logout.php">Déconnexion</a></li>
	<li id="configure_current_user">Paramètres</li>
	<li id="configure_other_users">Gérer les utilisateurs</li>
      </ul>

   <div id="user_links">

      <?php
	 $access_file = fopen("config/access.csv","r");
         $allowed_links = array();
         $allowed_links_cat = array();
         $allowed_links_name = array();
         $allowed_links_url = array();
         $cur_cat=FALSE;

	 if($access_file === FALSE) {
	   sso_errors();
           sso_log("error", "User links", "Unable to open config/access.csv");
	 } else {
	   //Get allowed links for the current user (and its groups)
	   while(!feof($access_file)) {
	     $buffer = fgets($access_file);
	     if(preg_match('/^#.*/i', $buffer) || preg_match('/^ *$/i', $buffer)) {
		 continue;
	     }

	     $line = explode(';', $buffer);
	     if(($line[0]==="" || $line[0]==="*" || check_user_group_in_list($_SESSION["groups"], explode(",", $line[0])) ) && ($line[1]==="" || $line[1]==="*" || in_array($_SESSION["login"], explode(",", $line[1])) )) {
	       //Previous version
	       //$allowed_links[] = array($line[2], $line[3], $line[4]);
	       $allowed_links[] = $line;
	       $allowed_links_cat[] = $line[2];
	       $allowed_links_name[] = $line[3];
	       $allowed_links_url[] = $line[4];
	     }
	   }
	   
	   //Sort tabs
	   //Source : http://www.developpez.net/forums/d1222763/php/langage/fonctions/trier-tableau-multidimensionnel-fonction-2e-dimension/
	   //Previous version
	   //array_multisort($allowed_links_cat, SORT_ASC, 
	   //		   $allowed_links_name, SORT_ASC, 
	   //	           $allowed_links_url, SORT_ASC, 
	   //		   $allowed_links);
	   array_multisort($allowed_links[2], SORT_ASC,
			   $allowed_links[3], SORT_ASC,
			   $allowed_links[4], SORT_ASC);
 
	   //Print allowed links sorted by categories and by link names
	   foreach($allowed_links as $link_datas) {
	     if($link_datas[2] !== $cur_cat) {
	       if($cur_cat !== FALSE) {
		 echo '</ul>';
		 echo '</div>';
	       }
	       $cur_cat = $link_datas[2];
	       echo '<div id="cat_link">';
	       echo '<h3 id="cat_name">'.$cur_cat.'</h3>';
	       echo '<ul id="link_list">';
	     }
	     echo '<li id="cur_link"><a href="browsing.php?url='.$link_datas[4].'&category='.$cur_cat.'">'.$link_datas[3].'</a></li>';
	   }
	   if($cur_cat !== FALSE)
	     echo '</ul>';
	 }
	 ?>
	 </div>
    </div>

    <?php include 'skin/'.$SSO_skin.'/footer.php'; ?>
  </body>
</html>
