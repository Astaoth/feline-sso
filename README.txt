* Pre-introduction
  The first version is still not released. Everything you will read here are the mainlines of this project. It is not already made !

* Introduction
  Feline SSO is a simple SSO made with PHP. After being identified througt a web portal, an user will redirected on a web page which will have a link for each website on which the user can be identified with this SSO. It operates only on links fro itself. The user authentication coulb be done from a simple text file, SQLite, MySQL, PostgreSQL, LDAP and Imap.

* Requirements
  - a web server
  - php 5
   
* Installation

* Feline SSO configuration
** General configuration parameters : 
   * Appreance
     - title
   The main title.
     - subtitle
   The sub-title of this portal.
     - logo
   The page logo.
     - skin
   The skin to apply.
   * Security
     - add_new_url_from_ui
     - add_new_url_allowed_groups
     - add_new_url_allowed_users
     - add_new_url_to_all_users_from_ui
     - add_new_url_to_all_users_allowed_users
     - add_new_url_to_all_users_allowed_groups
     - password_hash
     - backend
   * Text file :
     - file_text_path
   * SQLite :
     - sqlite_path
   * Imap :
     - imap_server
     - imap_port=
   * MySQL :
     - mysql_user_name
     - mysql_password
     - mysql_server
     - mysql_port
   * PostgreSQL :
     - postgresql_user_name
     - postgresql_password
     - postgresql_server
     - postgresql_port
   * LDAP :
     - ldap_server
     - ldap_port
     - ldap_group
** External website
   It's a simple csv file which stores the allowed access to each user or group. Websites will be sorted by categories. This one will be defined by the admins, and users if they are allowed to add websites.
   The different fields of this file will be : 
   - website url
   - website name
   - website category
   - a letter which defines if the authorisation access is done with the user name or group (U/G)
   - user name or group allowed
   With the Imap backend, the ACL can be done only with the user name.

* Skin
  Each skin will be stored inside the skin folder. There will be in a folder called with there name. The main css file is called "main.css"
