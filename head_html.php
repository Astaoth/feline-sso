<?php 
  /**** Load configuration ****/
include 'config/config.php';
   

/**** header generation ****/
//Title
if(isset($current_page_name)) {
  echo '<title>'.$SSO_title.' : '.$current_page_name.'</title>';
 }

//CSS
if(end(explode("/", explode("?" ,$_SERVER['REQUEST_URI'])[0])) === "browsing.php") {
  echo '<link rel="stylesheet" type="text/css" href="skin/'.$SSO_skin.'/browsing.css" />'; 
 }
 else {
   echo '<link rel="stylesheet" type="text/css" href="skin/'.$SSO_skin.'/main.css" />'; 
 }

//META
echo '<meta charset="UTF-8" />';
echo '<meta name="generator" content="Feline SSO" />';
echo '<meta name="robots" content="stop" />';
echo '<meta name="author" content="Astaoth - http://lotharedon.org" />';
echo '<meta name="description" content="Feline SSO - '.$SSO_title.' - '.$SSO_subtitle.'" />';


/**** Common usefull functions ****/
   
//Write an error message on the web page.
function sso_errors($html_text="") {
  global $SSO_timestamp_format;
  //Text inside the web pages
  echo '<h1>Something goes wrong !</h1>';
  echo '<p id="error">Something goes wrong. Please report this problem to the webmaster to '.$SSO_admin_contact.' with the url (web address) of this page and the exact time written below.';
  echo '<ul><li>url : http://'. $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'].'</li>';
  echo '<li>time : '.date($SSO_timestamp_format).'</li>';
  if($html_text !== "") echo '<li>additionnal informations : '.$html_text.'</li>';
  echo '</ul></p>';
     
  return true;
}

//Write something in the logfile
function sso_log($type, $title="", $description="") {
  global $SSO_timestamp_format;
  global $SSO_logfile_level;
  global $SSO_logfile;

  switch($type) {
  case "error":
    $type="ERROR";
    break;
  case "err":
    $type="ERROR";
    break;
  case "warning":
    $type="WARNING";
    break;
  case "warn":
    $type="WARNING";
    break;
  case "info":
    $type="INFO";
    break;
  case "debug":
    $type="DEBUG";
    break;
  case "dbg":
    $type="DEBUG";
    break;
  }

  if(($SSO_logfile_level != 0 && $type === "ERROR") || ($SSO_logfile_level > 1 && $type === "WARNING") || ($SSO_logfile_level > 2 && $type === "INFO") || ($SSO_logfile_level > 3 && $type !== "DEBUG") || ($SSO_logfile_level > 4)) {
    $logfile = fopen($SSO_logfile, "a");
    if($logfile == FALSE) {
      echo '<h1><strong>CANNOT OPEN THEN LOGFILE. PLEASE MAKE SUR PHP CAN WRITE INSIDE.</strong></h1>';
      return false;
    } else {
      fwrite($logfile, date($SSO_timestamp_format)." - [".$type."] ".$title." - ".$description."\n");
      fclose($logfile);
      return true;
    }
  }
  else return false;
}

//Check the user connection status and redirect him to the right page if he has a wrong status
function sso_check_status($status) {
  //Check if the user is connected or redirect him to the login form
  if($status === "connected") {
    if(!isset($_SESSION["login"]) || !isset($_SESSION["password"]))
      header('Location: index.php');
  }
  
  //Check if the user is disconnected or redirect him to the welcome page
  if($status === "disconnected") {
    if(isset($_SESSION["login"]) && isset($_SESSION["password"]))
      header('Location: accueil.php');
  }
}


//Build an SQL handler
//Type : request on site or user datas
function sql_open($type) {
  $reslt = FALSE;
  global ${"SSO_".$type."_backend"};
  
  //////////////////////// SQLITE
  if(${"SSO_".$type."_backend"} === "sqlite") {
    global ${"SSO_".$type."_sqlite_path"};
      
    if((!extension_loaded("sqlite3")) && (!extension_loaded("pdo_sqlite"))) {
      sso_errors();
      sso_log("error", "PHP configuration", "PHP SQLite 3 and PDO SQLite extensions and not loaded. Please enable one of them or choose an other backend.");
      return $reslt = FALSE;
    }
    if(!isset(${"SSO_".$type."_sqlite_path"})) {
      sso_errors();
      sso_log("error", "Backend configuration", "Undefined SSO_$type_sqlite_path");
      return $reslt = FALSE;
    }
    if(!file_exists(${"SSO_".$type."_sqlite_path"})) {
      sso_errors();
      sso_log("error", "Backend configuration", "SSO_$type_sqlite_path doesn't exist.");
      return $reslt = FALSE;
    }
    
    //////////// With sqlite3
    if(extension_loaded("sqlite3")) {
      if($SSO_logfile_level >= 5)
	echo '<br />SQLite3 extension will be used for the SQLite request.<br />';
      sso_log("notice", "PHP extension", "SQlite3 extension will be used for the SQLite request.");
      
      $db = sqlite_open(${"SSO_".$type."_sqlite_path"}, 0666, $sqliteerror);
      if($sqliteerror) {
	echo $sqliteerror.'<br />';
      }
      if(!$db) {
	sso_errors();
	sso_log("error", "SQLite", "Unable to open SSO_$type_sqlite_path with sqlite3.");
	return $reslt = FALSE;
      }
      sso_log("notice", "SQLite", "Connection success with sqlite3.");
      return $reslt = $db;
    }
    //////////// With pdo_sqlite
    else if(extension_loaded("pdo_sqlite")) {
      if($SSO_logfile_level >= 5)
	echo '<br />PDO SQlite extension will be used for the SQLite request.<br />';
      sso_log("notice", "PHP extension", "PDO SQLite extension will be used for the SQLite request.");
      
      try {
	$db = new PDO("sqlite:".${"SSO_".$type."_sqlite_path"});
	sso_log("notice", "SQLite", "Connection success with pdo_sqlite.");
	return $reslt = $db;
      } catch (PDOException $e) {
	sso_errors();
	//      sso_log("error", "SQLite", "Unable to open SSO_$type_sqlite_path with pdo_sqlite  (". e->getMessage().").");
	sso_log("error", "SQLite", "Unable to open SSO_$type_sqlite_path with pdo_sqlite.");
	return $reslt = FALSE;
      }     
    }
  }
	
  //////////////////////// MYSQL
  else if(${"SSO_".$type."_backend"} === "mysql") {
    global ${"SSO_".$type."_mysql_user_name"};
    global ${"SSO_".$type."_mysql_password"};
    global ${"SSO_".$type."_mysql_server"};
    global ${"SSO_".$type."_mysql_base"};
    global ${"SSO_".$type."_mysql_port"};
    
    if((!extension_loaded("pdo_mysql")) && (!extension_loaded("mysqli"))) {
      sso_errors();
      sso_log("error", "PHP configuration", "MySQLi and PDO_MySQL extensions are not loaded. Please enable one of them or choose an other backend.");
      return $reslt = FALSE;
    }
    if(!isset(${"SSO_".$type."_mysql_user_name"})) {
      sso_errors();
      sso_log("error", "Backend configuration", "Undefined SSO_$type_mysql_user_name");
      return $reslt = FALSE;
    }
    if(!isset(${"SSO_".$type."_mysql_password"})) {
      sso_errors();
      sso_log("error", "Backend configuration", "Undefined SSO_$type_mysql_password");
      return $reslt = FALSE;
    }
    if(!isset(${"SSO_".$type."_mysql_server"})) {
      sso_errors();
      sso_log("error", "Backend configuration", "Undefined SSO_$type_mysql_server");
      return $reslt = FALSE;
    }
    if(!isset(${"SSO_".$type."_mysql_base"})) {
      sso_errors();
      sso_log("error", "Backend configuration", "Undefined SSO_$type_mysql_base");
      return $reslt = FALSE;
    }
    if(!isset(${"SSO_".$type."_mysql_port"})) {
      sso_errors();
      sso_log("error", "Backend configuration", "Undefined SSO_$type_mysql_port");
      return $reslt = FALSE;
    }
    
    //////////// With mysqli 
    if(extension_loaded("mysqli")) {
      if($SSO_logfile_level >= 5)
	echo '<br />MySQLi extension will be used for the MySQL request.<br />';
      sso_log("notice", "PHP extension", "MySQLi extension will be used for the MySQL request.");
      $db = new mysqli(${"SSO_".$type."_mysql_server"}, ${"SSO_".$type."_mysql_user_name"}, ${"SSO_".$type."_mysql_password"}, ${"SSO_".$type."_mysql_base"}, ${"SSO_".$type."_mysql_port"});
      
      if ($mysqli->connect_errno) {
	sso_errors();
	sso_log("error", "MySQL", "Conection to MySQL db with mysqli failed : (". $mysqli->connect_errno.") ".$mysqli->connect_error);
	return $reslt = FALSE;
      } else {
	sso_log("notice", "MySQL", "Connection success with mysqli.");
	return $reslt = $db;
      }
    } 
    //////////// With PDO MySQL
    else if(extension_loaded("pdo_mysql")){
      if($SSO_logfile_level >= 5)
	echo '<br />PDO MySQL extension will be used for the MySQL request.<br />';
      sso_log("notice", "PHP extension", "PDO MySQL extension will be used for the MySQL request.");
    }

    try {
      $db = new PDO("mysql:host=".${"SSO_".$type."_mysql_server"}.";port=".${"SSO_".$type."_mysql_port"}.";dbname=".${"SSO_".$type."_mysql_base"}, ${"SSO_".$type."_mysql_user_name"}, ${"SSO_".$type."_mysql_password"});
      sso_log("notice", "MySQL", "Connection success with pdo_mysql.");
      return $reslt = $db;
    } catch (PDOException $e) {
      sso_errors();
      //      sso_log("error", "MySQL", "Conection to MySQL db with pdo_mysql failed (". e->getMessage().").");
      sso_log("error", "MySQL", "Conection to MySQL db with pdo_mysql failed.");
      return $reslt = FALSE;
    }
  }
  //////////////////////// PostgreSQL
  else if(${"SSO_".$type."_backend"} === "postgresql") {
    global ${"SSO_".$type."_postgresql_user_name"};
    global ${"SSO_".$type."_postgresql_password"};
    global ${"SSO_".$type."_postgresql_server"};
    global ${"SSO_".$type."_postgresql_base"};
    global ${"SSO_".$type."_postgresql_port"};
    
    if((!extension_loaded("pdo_postgresql")) && (!extension_loaded("pgsql"))) {
      sso_errors();
      sso_log("error", "PHP configuration", "pgsql and PDO_PostgreSQL extensions are not loaded. Please enable one of them or choose an other backend.");
      return $reslt = FALSE;
    }
    if(!isset(${"SSO_".$type."_postgresql_user_name"})) {
      sso_errors();
      sso_log("error", "Backend configuration", "Undefined SSO_$type_postgresql_user_name");
      return $reslt = FALSE;
    }
    if(!isset(${"SSO_".$type."_postgresql_password"})) {
      sso_errors();
      sso_log("error", "Backend configuration", "Undefined SSO_$type_postgresql_password");
      return $reslt = FALSE;
    }
    if(!isset(${"SSO_".$type."_postgresql_server"})) {
      sso_errors();
      sso_log("error", "Backend configuration", "Undefined SSO_$type_postgresql_server");
      return $reslt = FALSE;
    }
    if(!isset(${"SSO_".$type."_postgresql_base"})) {
      sso_errors();
      sso_log("error", "Backend configuration", "Undefined SSO_$type_postgresql_base");
      return $reslt = FALSE;
    }
    if(!isset(${"SSO_".$type."_postgresql_port"})) {
      sso_errors();
      sso_log("error", "Backend configuration", "Undefined SSO_$type_postgresql_port");
      return $reslt = FALSE;
    }
    
    //////////// With pgsql 
    if(extension_loaded("pgsql")) {
      if($SSO_logfile_level >= 5)
	echo '<br />pgsql extension will be used for the PostgreSQL request.<br />';
      sso_log("notice", "PHP extension", "pgsql extension will be used for the PostgreSQL request.");
      $db = pg_connect("host=".${"SSO_".$type."_postgresql_server"}." port=".${"SSO_".$type."_postgresql_port"}." dbname=".${"SSO_".$type."_postgresql_base"}." user=".${"SSO_".$type."_postgresql_user_name"}." password=".${"SSO_".$type."_postgresql_password"});
      
      if ($db == FALSE) {
	sso_errors();
	sso_log("error", "PostgreSQL", "Conection to PostgreSQL db with pgsql failed.");
	return $reslt = FALSE;
      } else {
	sso_log("notice", "PostgreSQL", "Connection success with pgsql.");
	return $reslt = $db;
      }
    } 
    //////////// With PDO PostgreSQL
    else if(extension_loaded("pdo_postgresql")){
      if($SSO_logfile_level >= 5)
	echo '<br />PDO Postgresql extension will be used for the Postgresql request.<br />';
      sso_log("notice", "PHP extension", "PDO Postgresql extension will be used for the Postgresql request.");
    }
    
    try {
      $db = new PDO("postgresql:host=".${"SSO_".$type."_postgresql_server"}.";port=".${"SSO_".$type."_postgresql_port"}.";dbname=".${"SSO_".$type."_postgresql_base"}, ${"SSO_".$type."_postgresql_user_name"}, ${"SSO_".$type."_postgresql_password"});
      sso_log("notice", "PostgreSQL", "Connection success with pdo_postgresql.");
      return $reslt = $db;
    } catch (PDOException $e) {
      sso_errors();
      //      sso_log("error", "PostgreSQL", "Conection to PostgreSQL db with pdo_postgresql failed (". e->getMessage().").");
      sso_log("error", "PostgreSQL", "Conection to PostgreSQL db with pdo_postgresql failed.");
      return $reslt = FALSE;
    }
  }
  //////////////////////// ELSE
  else {
    sso_errors();
    sso_log("error", "Backend configuration", "Undefined SSO_$type_backend");
    return $reslt = FALSE;
  }
  
  return $reslt;
}

//Execute an SQL request
//Type : request on site or user datas
//Handler : database handler (resulting of sql_open)
//Request : a string containing the request
function sql_request($type, $handler, $request) {
  $reslt = "";
  global ${"SSO_".$type."_backend"};
  if(! $handler) {
    sso_errors();
    sso_log("error", "SQL", "Undefined SQL handler.");
    return $reslt = FALSE;
  }

  ////////////////////// SQLITE && php_sqlite3
  if((${"SSO_".$type."_backend"} === "sqlite") &&(extension_loaded("sqlite3"))){
    if($SSO_logfile_level >= 5)
      echo '<br />SQLite request will be executed<br />';
    $reslt = sqlite_array_query($handler, sqlite_escape_string($request), SQLITE_BOTH);
    if($SSO_logfile_level >= 5)
      echo 'Request result dump : <br />';
    if($SSO_logfile_level >= 5)
      var_dump($reslt);
  } 
  ////////////////////// MYSQL && mysqli
  else if((${"SSO_".$type."_backend"} === "mysql") && (extension_loaded("mysqli"))) {
    if($SSO_logfile_level >= 5)
      echo '<br />MySQL request will be executed<br />';
    $reslt = $handler->query($request);
    if(($reslt != TRUE) && ($reslt != FALSE))
      $reslt = mysqli_fetch_all($reslt,  MYSQLI_BOTH);
    if($SSO_logfile_level >= 5)
      echo 'Request result dump : <br />';
    if($SSO_logfile_level >= 5)
      var_dump($reslt);
  }
  ////////////////////// PostreGreSQL && pgsql
  else if((${"SSO_".$type."_backend"} === "postgresql") && (extension_loaded("pgsql"))) {
    if($SSO_logfile_level >= 5)
      echo '<br />PostgreSQL request will be executed<br />';
    $reslt = pg_query($handler, $request);
    if(($reslt != TRUE) && ($reslt != FALSE))
      $reslt = pg_fetch_all($reslt); //return associativ array
    if($SSO_logfile_level >= 5)
      echo 'Request result dump : <br />';
    if($SSO_logfile_level >= 5)
      var_dump($reslt);
  }
  ////////////////////// PDO
  else if( ((${"SSO_".$type."_backend"} === "sqlite") && (extension_loaded("pdo_sqlite"))) || ((${"SSO_".$type."_backend"} === "mysql") && (extension_loaded("pdo_mysql"))) || ((${"SSO_".$type."_backend"} === "postgresql") && (extension_loaded("pdo_postgresql"))) ) {
    if($SSO_logfile_level >= 5)
      echo '<br />PDO request will be executed<br />';

    $reslt = $handler->query($request);
    if($reslt != FALSE)
      $reslt = $reslt->fetchAll(PDO::FETCH_BOTH);

    if($SSO_logfile_level >= 5)
      echo 'Request result dump : <br />';
    if($SSO_logfile_level >= 5)
      var_dump($reslt);
  }
  ////////////////////// ELSE
  else{
    sso_errors();
    sso_log("error", "SQL", "Backend not supported OR no recognised php extension for the choosen backend supported.");
    $reslt = FALSE;
  }
  
  return $reslt;
}


//Build a request handler. Will be used to make requests on file, LDAP, or SQL db in the same way
//NB : Imap request (usables only for auth) are not handled here
//$type : db type (user or site)
//return a request handler
function request_handler_make($type) {
  $handler = TRUE;
  global ${"SSO_".$type."_backend"};
  
   ///////////////////////////////////////// FILE_TEXT
   if(${"SSO_".$type."_backend"} == "file_text") {
     global ${"SSO_".$type."_file_text_path"};
     if(!isset(${"SSO_".$type."_file_text_path"})) {
       sso_errors();
       sso_log("error", "Backend configuration", "Undefined SSO_".$type."_file_text_path");
       $handler = FALSE;
     }
     if($handler != FALSE) {
       $handler = fopen(${"SSO_".$type."_file_text_path"}, "rw");
       if($handler == FALSE) {
	 sso_errors();
	 sso_log("error", "Backend configuration", "Unable to open SSO_$type_file_text_path");
       }
     }
   }
   ///////////////////////////////////////// SQL
   else if((${"SSO_".$type."_backend"} == "sqlite") || (${"SSO_".$type."_backend"} == "mysql") || (${"SSO_".$type."_backend"} == "postgresql")) {
     $handler = sql_open($type);
   }
   ///////////////////////////////////////// LDAP
   //Unsuported at the moment

   ///////////////////////////////////////// ELSE
   else {
     sso_errors();
     sso_log("error", "Backend configuration", "Unreconised backend SSO_$type_backend.");
     $handler = FALSE;
   }
   
   return $handler;
}
 
//Exec a request on a given request_handler.
//$type : type of datas (user, site)
//$handler : request handler made by request_handler_exec
//$reqtype : type of the request (search, add, del, update)
//$datas : array with the data request : 
//         - add : the tuple data ; 
//         - del : the tuple datas which we want to del for a better match (mixed assoc/numerical array)
//         - search : tuple data for a better match (mixed assoc/numerical array) EG : {username : "username", email : "", password : "password", group : {0 : "group1", 1 : "group2"}}.
//         - update : in the array, each data tuple is an array with the old and new datas. EG : {username : {0 : username, 1 : username}, password : {0 : oldpassword, 1 : newpassword}, groups : {0 : old group list, 1 : new group list}}. The first data set will be used like with the search reqtype. Next, they will be replaced by the second data set.
//    About del/search : will match datas with all matching fields. A field will be matching if all the datas matches
// /!\ WITH SQL : each key must be an attribute name !!!
// With text file, the keys are not used but instead the column number
 //return : TRUE, FALSE, or array with requested datas
function request_handler_exec($type, $handler, $reqtype, $data) {
  $reslt = FALSE;
  global ${"SSO_".$type."_backend"};

  ///////////////////////////////////////// FILE_TEXT
  if(${"SSO_".$type."_backend"} == "file_text") {
    //Sert à avoir les numéros des colonnes utilisées
    if($type == "user")
      $columns = array("username", "email", "password", "groupList");
    else if($type == "site")
      $columns = array("groupList", "userList", "category", "name", "url", "auth", "username", "password");
    else {
      sso_errors();
      sso_log("error", "Function Parameter", "request_handler_exec : unreconised parameter reqtype \"$reqtype\".");
      return $reslt = FALSE;
    }
    
    if($reqtype == "search") {
      $reslt = array();

      while(!feof($handler)) { //For each line in the file
	$validLine = TRUE; //Token which stores if the line as the required data
	$line = explode(';',fgets($handler));
	//echo "<br> newline";
	//echo "<br>";
	//var_dump($line);
	//echo "<br>";

	//For a given line, compare the data with the given ones
	foreach($data as $key => $val) { //For each required data
	  //echo "key : ".$key." - value : ".$val."<br>";
	  for($colID = 0 ; $colID <= count($columns) ; $colID++) {
	    //echo "columns(colID) : ".$columns[$colID]."<br>";
	    if($columns[$colID] == $key)
	      break;
	  }

	  if($colID >= count($columns)) { //if the data key doesn't match a column name
	    sso_errors();
	    sso_log("error", "Function Parameter", "request_handler_exec : unreconised data type (one of the keys of the data parameter) \"$key\".");
	    return $reslt = FALSE;
	  }

	  //if((($type == "site") && (($colID == 0) || ($colID == 1))) || (($type == "user") && ($colID == 3))) { //old version, not easy to evolve
	  //if(ereg("List$", $key)) { never used, based on the code convention
	  if(is_array($val)) { //If array, each data are looked fo (AND)
	    //echo "val : ";
	    //var_dump($val);

	    //echo "<br>stored data : ";
	    //var_dump(array_map('trim',explode(',', $line[$colID])));

	    
	    // Compare given data with stored data as arrays
	    $tmp = array_diff($val, array_map('trim',explode(',', $line[$colID])));
	    //echo "<br>diff : ";
	    //var_dump($tmp);

	    if($tmp != array()) { //If the current line doesn't have required data, go to next line
	      //echo "<br>Not a valid line <br>";
	      $validLine = FALSE;
	      break;
	    }	    
	  } else { //If not array
	    //echo "NOT ARRAY<br>";
	    //echo "line[colID] : ".$line[$colID]."<br>";
	    if($line[$colID] != $val) {
	      //echo "Not a valid line <br>";
	      $validLine = FALSE;
	      break;
	    }
	  }
	} //foreach end

	if($validLine) {//if the line matches all requirements, we add it to the reslt
	  $reslt[] = $line;
	}
      } //while end
      rewind($handler);
      return $reslt;
    } else if($reqtype == "add") {
    } else if($reqtype == "del") {
    } else if($reqtype == "update") {
    } else {
	sso_errors();
	sso_log("error", "Function Parameter", "request_handler_exec : unreconised parameter reqtype \"$reqtype\".");
	return $reslt = FALSE;
    }
  }

  ///////////////////////////////////////// SQL
  else if((${"SSO_".$type."_backend"} == "sqlite") || (${"SSO_".$type."_backend"} == "mysql") || (${"SSO_".$type."_backend"} == "postgresql")) {
    if($reqtype == "search") {
      $req = "";
      if($type == "user") { //All user data
	$req = "SELECT * FROM sso_users LEFT OUTER JOIN sso_groups ON sso_users.username = sso_groups.username ";
      } else if($type == "site") { //All site data
	$req = "SELECT * FROM sso_sites LEFT OUTER JOIN sso_sites_allowed_users ON sso_sites.index = sso_allowed_users.site LEFT OUTER JOIN sso_sites_allowed_groups ON sso_allowed_users.site = sso_sites_allowed_groups.site";
      } else {
	sso_errors();
	sso_log("error", "Function parameter", "request_handler_exec : unreconised parameter type \"$type\".");
	return $reslt = FALSE;
      }
	
      //SQL regexp symbol in the WHERE clause generation
      if(${"SSO_".$type."_backend"} == "postgresql")
	$regexp = "~";
      else if((${"SSO_".$type."_backend"} == "sqlite") || (${"SSO_".$type."_backend"} == "mysql"))
	$regexp = "REGEXP";
      
      $where="";
      foreach($datas as $key => $val) {
	if($where != "") //if there is already a "where" clause
	  $where .= " AND ";
	if(is_array($val)) { //if given data is an array
	  $tmp="";
	  foreach($val as $v) {
	    if($tmp == "")
	      $tmp .= "((";
	    else
	      $tmp .= ")|(";
	    $tmp .= $v;
	  }
	  $tmp .= "))";
	} else { //if given data is not an array
	  $tmp = $val;
	}
	
	$where .= $key.' '.$regexp.' '.$tmp;
      }
      $req .= $where;
      $req .= ";";
    } else if($reqtype == "add") {
    } else if($reqtype == "del") {
    } else if($reqtype == "update") {
    } else {
	sso_errors();
	sso_log("error", "Function Parameter", "request_handler_exec : unreconised parameter reqtype \"$reqtype\".");
	return $reslt = FALSE;
    }
    return $reslt = sql_request($type, $handler, $req);
  }
  ///////////////////////////////////////// LDAP
  //Unsuported at the moment
  
  ///////////////////////////////////////// ELSE
  else {
    sso_errors();
    sso_log("error", "Backend configuration", "Unreconised backend SSO_$type_backend.");
    $reslt = FALSE;
  }
  
  return $reslt;
}

?>
