<?php session_start();?>
<html>
  <head>
    <?php $current_page_name = "Identification";
	  include 'head_html.php'; 
	  sso_check_status("disconnected"); ?>
<!--    <?php echo '<title>'.$SSO_title.' : identification</title>'; ?>-->
  </head>
  <body id="form">
    <?php include 'skin/'.$SSO_skin.'/header.php'; ?>

    <div id="form">
      <?php
	 if(isset($_GET["msg"]) && $_GET["msg"] !== "") {
	   echo '<span id="msg">'.$_GET["msg"].'</span>';
	 }
	 ?>
      
      <form action="login.php" method="post">
	<span id="form_text">Identifiant</span><input type="text" name="login">
	<span id="form_text">Mot de passe</span><input type="password" name="password">
	<input type="submit" value="Connexion">
      </form>
      <a href="lost_passord.php">Signaler l'oublie du mot de passe.</a>
    </div>

    <?php include 'skin/'.$SSO_skin.'/footer.php'; ?>
  </body>
</html>
