<!--
TODO
cette page permettra de naviguer au sein de d'autres sites géré par le sso
Elle y fera l'authentification automatique, affichera un bandeau de navigation, et affichera une barre d'url du site inclu

-->
<?php session_start(); ?>
<html>
  <head>
    <?php include 'head_html.php'; 
        sso_check_status("connected"); ?>
    <?php echo '<title>'.$SSO_title.' : browsing</title>'; ?>
    <?php $_SESSION["current_url"] = $_GET["url"];
	  $_SESSION["current_category"] = $_GET["cat"];
	  ?>

  </head>

  <body id="browsing">
<!--
    <ul id="browsing">
      <li id="home_button">Home</li>
      <li id="cat_button"><?php /*TODO : current cat name*/ ?>
	<ul id="cat_menu"><?php /*TODO : cat menu generator*/?>
	</ul></li>
      <?php /*TODO :  button current cat generator
	    ** if too much, menu "others/more" */ ?>
    </ul>
-->

    <div id="browsing">
      
      <ul><li><a href="index.php" id="home_button">Home</a></li>
	<li><a href="logout.php" id="logout_button">Déconnexion</a></li>
	<li><?php echo '<a href="'.$_SESSION["current_url"].'" id="site_access_button">Accès direct au site</a>'; ?></li>
      </ul>
    </div>
    
    
    <div id="included_page">
      <iframe id="browsing" src="curl_request.php">
      </iframe>
    </div>
    
    
  </body>
</html>
