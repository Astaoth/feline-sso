<?php session_start(); ?>
<html>
  <head>
    <?php $current_page_name = "Mot de passe perdu";
	  include 'head_html.php'; 
          sso_check_status("connected"); ?>
  </head>

  <body id="lost_psw">
    <?php include 'skin/'.$SSO_skin.'/header.php'; ?>

    <div id="lost_psw">
      <h1>Mot de passe perdu</h1>
      <?php
	 if(!isset($_POST["email"])) {
	   echo '<form action="lost_password.php" method="post">
	         <span id="form_text">Identifiant</span><input type="text" name="login" />
	         <span id="form_text">Adresse de courriel</span><input type="text" name="email" />
	         <input type="submit" value="Envoyer" />
                 </form>';
          echo '<p>NB : Si votre nom d'utilisateur est votre adresse de courriel, veuillez entrer celle-ci dans les deux champs ci-dessus.</p>';
      } else {
        check_user_password($_POST["login"], $_POST["email"]);
        
        

      }
	 

      
      ?>
    </div>

    <?php include 'skin/'.$SSO_skin.'/footer.php'; ?>
  </body>
</html>
